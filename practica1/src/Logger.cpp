#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define TAM_BUFFER 60

using namespace std;

int main(){
	int pipe;
	pipe=mkfifo("/tmp/tuberia",0777);
	if(pipe==-1){
		perror("Fallo al crear la tuberia p2");
		exit(1);
	}
	int fd=open("/tmp/tuberia",O_RDONLY);
	if(fd==-1){
		perror("Fallo al abrir la tuberia p2");
		exit(1);
	}

	while(1){
		char buffer[TAM_BUFFER];
		int eread=read(fd,buffer,sizeof(buffer));
		if(eread==-1){
			perror("Fallo en la lectura de la tuberia p2");
			exit(1);
		}
		else if(eread==0){
			if(close(fd)==-1){
				perror("Fallo al cerrar la tuberia");
				exit(1);
			}
			unlink("/tmp/tuberia");
			exit(0);
		}
		else
			//cout<<buffer<<endl;
			printf("%s\n",buffer);
	}
	
	
	return 0;
}
